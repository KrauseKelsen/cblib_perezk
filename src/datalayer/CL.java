/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datalayer;

import businesslayer.usuario.Coleccionista;
import businesslayer.item.Item;
import businesslayer.oferta.Oferta;
import businesslayer.usuario.Usuario;
import businesslayer.usuario.Vendedor;
import datalayer.Conexion;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Luis
 */
public class CL {

    private final ArrayList<Usuario> usuarios;
    private final ArrayList<Item> items;
    private final ArrayList<Oferta> ofertas;

    public CL() {
        usuarios = new ArrayList<>();
        items = new ArrayList<>();
        ofertas = new ArrayList<>();
    }

    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }

    public ArrayList<Item> getItem() {
        return items;
    }

    public ArrayList<Oferta> getOferta() {
        return ofertas;
    }
    
    public String moderadorExistente(){
        String validacion = "";
        try {
            Statement st;//Almacena la consulta SQL
            st = Conexion.dbConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Moderador");
            while (rs.next()) {

                validacion = rs.getString("idUsuario");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        if (!"".equals(validacion)) {
            return validacion;
        }
        return "";
    }

    public void registrarUsuario(String nombre, String identificacion,
            LocalDate bornIn, int edad, String correo,
            String password) throws SQLException {
        Date miDate = Date.valueOf(bornIn);
        try {
            String sql = "INSERT INTO Usuario VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement st = Conexion.dbConexion().prepareStatement(sql);
            st.setString(1, identificacion);
            st.setString(2, nombre);
            st.setDate(3, miDate);
            st.setInt(4, edad);
            st.setString(5, correo);
            st.setString(6, password);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void registrarColeccionista(String identificacion, int puntuacion,
            String provincia, String canton, String distrito, String direccion)
            throws SQLException {
        //            ArrayList<String> intereses, ArrayList<Item> objetosPropios, 
        try {
            String sql = "INSERT INTO Coleccionista VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement st = Conexion.dbConexion().prepareStatement(sql);
            st.setString(1, identificacion);
            st.setInt(2, puntuacion);
            st.setString(3, provincia);
            st.setString(4, canton);
            st.setString(5, distrito);
            st.setString(6, direccion);
            st.executeUpdate();
            System.out.println("Todo bien");
        } catch (SQLException e) {
            System.out.println(e.getMessage() + "Aqui está el error perrito");
        }
    }

    public void registrarVendedor(String identificacion, int puntuacion,
            String provincia, String canton, String distrito, String direccion)
            throws SQLException {
        try {
            String sql = "INSERT INTO Vendedor VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement st = Conexion.dbConexion().prepareStatement(sql);
            st.setString(1, identificacion);
            st.setInt(2, puntuacion);
            st.setString(3, provincia);
            st.setString(4, canton);
            st.setString(5, distrito);
            st.setString(6, direccion);
            st.executeUpdate();
            System.out.println("Todo bien");
        } catch (SQLException e) {
            System.out.println(e.getMessage() + "Aqui está el error perrito");
        }
    }

    public void registrarModerador(String identificacion)
            throws SQLException {
        try {
            String sql = "INSERT INTO Moderador VALUES (?)";
            PreparedStatement st = Conexion.dbConexion().prepareStatement(sql);
            st.setString(1, identificacion);
            st.executeUpdate();
            System.out.println("Todo bien");
        } catch (SQLException e) {
            System.out.println(e.getMessage() + "Aqui está el error perrito");
        }
    }

    public void registrarObjeto(String identificacion, String ID, String nombre, String descripcion,
            LocalDate fechaCompra, String antiguedad, String estado) {
        Date miDate = Date.valueOf(fechaCompra);
        try {
            String sql = "INSERT INTO Item VALUES (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement st = Conexion.dbConexion().prepareStatement(sql);
            st.setString(1, ID);
            st.setString(2, nombre);
            st.setString(3, descripcion);
            st.setString(4, estado);
            st.setDate(5, miDate);
            st.setString(6, antiguedad);
            st.setString(7, identificacion);
            st.executeUpdate();
            System.out.println("Todo bien");
        } catch (SQLException e) {
            System.out.println(e.getMessage() + "Aqui está el error perrito");
        }
    }

    public void registrarOferta(Oferta miOferta) {
        ofertas.add(miOferta);
    }

    public String validarUsuario(String identificacion) {

        String validacion = "";
        try {
            Statement st;//Almacena la consulta SQL
            st = Conexion.dbConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT identificacion FROM Usuario WHERE identificacion = " + "'" + identificacion + "'");
            while (rs.next()) {

                validacion = rs.getString("identificacion");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        if (validacion.equalsIgnoreCase("")) {
            return "";

        }

        return validacion;
    }

    public String validarCorreo(String correo) {
        String validacion = "";
        try {
            Statement st;//Almacena la consulta SQL
            st = Conexion.dbConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT correo FROM Usuario WHERE correo = " + "'" + correo + "'");
            while (rs.next()) {

                validacion = rs.getString("correo");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        if (validacion.equalsIgnoreCase("")) {
            return "";

        }

        return validacion;
    }

    public String validarObjeto(String ID) {
        String validacion = "";
        try {
            Statement st;//Almacena la consulta SQL
            st = Conexion.dbConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT idItem FROM Item WHERE idItem = " + "'" + ID + "'");
            while (rs.next()) {

                validacion = rs.getString("idItem");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        if (validacion.equalsIgnoreCase("")) {
            return "";

        }

        return validacion;
    }

    public int calcularEdad(LocalDate fechaNacimiento) throws IOException {
        int years = 0;
        Date miDate = Date.valueOf(fechaNacimiento);
        try {

            Statement st;//Almacena la consulta SQL
            st = Conexion.dbConexion().createStatement();
            ResultSet rs = st.executeQuery("SELECT [dbo].[fn_calcularEdad]" + "('" + miDate + "')");
            while (rs.next()) {

                years = rs.getInt("");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return years;
    }

    public String calcularAntiguedad(LocalDate fechaCompra) {
        String antiguedad = "";
        try{
        LocalDate fechaActual = LocalDate.now();

        int years = fechaActual.getYear() - fechaCompra.getYear();
        int months = fechaActual.getMonthValue() - fechaCompra.getMonthValue();
        int days = fechaActual.getDayOfMonth() - fechaCompra.getDayOfMonth();

        if (months < 0 || (months == 0 && days < 0)) {
            years--;
        }

        months *= -1;
        days *= -1;
        antiguedad = "Días: " + days + ". " + "Meses: " + months + ". "
                + "Años: " + years + ".";
        }catch(Exception e){
            
        }
        return antiguedad;
    }
}
