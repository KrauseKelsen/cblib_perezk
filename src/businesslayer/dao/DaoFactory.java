/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslayer.dao;

import businesslayer.item.IItemDAO;
import businesslayer.oferta.IOfertaDAO;
import businesslayer.orden.IOrdenDAO;
import businesslayer.subasta.ISubastaDAO;
import businesslayer.usuario.IUsuarioDAO;

/**
 *
 * @author Luis
 */
//DEFINE QUE MOTOR DE BASE DE DATOS VAMOS A USAR
public abstract class DaoFactory {

    //Va tener dos variables estaticas, una por motor de DB
    public static final int SQLSERVER = 1;

    //El factory es un PATRÓN (curso de patrones) un patrón como viene en el lab
    //es una solución ya probada para aun problema especifico
    //El patron Factory es una fabrica de objetos
    public static DaoFactory getDaoFactory(int factory) {
        //Retorna el factory especifico que recibe
        switch (factory) {
            case SQLSERVER:
                return new SqlServerDaoFactory();
            default:
                return null;
        }
    }

    public abstract IUsuarioDAO getUsuarioDAO();
    public abstract IItemDAO getItemDAO();
    public abstract IOfertaDAO getOfertaDAO();
    public abstract IOrdenDAO getOrdenDAO();
    public abstract ISubastaDAO getSubastaDAO();
}
