/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslayer.dao;

import businesslayer.item.IItemDAO;
import businesslayer.item.ItemDAO;
import businesslayer.oferta.IOfertaDAO;
import businesslayer.oferta.OfertaDAO;
import businesslayer.orden.IOrdenDAO;
import businesslayer.orden.OrdenDAO;
import businesslayer.subasta.ISubastaDAO;
import businesslayer.subasta.SubastaDAO;
import businesslayer.usuario.IUsuarioDAO;
import businesslayer.usuario.UsuarioDAO;


/**
 *
 * @author Luis
 */
//ESTE FACTORY VA RETORNAR TODOS LOS MULTIS (TODOS LOS OBJ DE LA CONSULTA SQL) DE CADA CLASE
public class SqlServerDaoFactory extends DaoFactory {

    public SqlServerDaoFactory() {
    }

    public IItemDAO getItemDAO() {
        return new ItemDAO();
    }
    
    public ISubastaDAO getSubastaDAO() {
        return new SubastaDAO();
    }
    
    public IOfertaDAO getOfertaDAO() {
        return new OfertaDAO();
    }
    
    public IUsuarioDAO getUsuarioDAO() {
        return new UsuarioDAO();
    }
    
    public IOrdenDAO getOrdenDAO() {
        return new OrdenDAO();
    }
}
