

package businesslayer.orden;
import businesslayer.usuario.Coleccionista;
import businesslayer.item.Item;
import java.time.LocalDate;
import java.util.ArrayList;

public class Orden {
    private Coleccionista ganador;
    private LocalDate fechaOrden;
    private ArrayList <Item> objetos;
    private int precioTotal;

    public Orden() {
    }

    public Orden(Coleccionista ganador, LocalDate fechaOrden, ArrayList<Item> objetos, int precioTotal) {
        this.ganador = ganador;
        this.fechaOrden = fechaOrden;
        this.objetos = objetos;
        this.precioTotal = precioTotal;
    }

    public Coleccionista getGanador() {
        return ganador;
    }

    public void setGanador(Coleccionista ganador) {
        this.ganador = ganador;
    }

    public LocalDate getFechaOrden() {
        return fechaOrden;
    }

    public void setFechaOrden(LocalDate fechaOrden) {
        this.fechaOrden = fechaOrden;
    }

    public ArrayList<Item> getObjetos() {
        return objetos;
    }

    public void setObjetos(ArrayList<Item> objetos) {
        this.objetos = objetos;
    }

    public int getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(int precioTotal) {
        this.precioTotal = precioTotal;
    }

    @Override
    public String toString() {
        return "Orden: " 
                + "Ganador: " 
                + ganador + ".\n"
                + "Fecha de la orden: " 
                + fechaOrden + ".\n"
                + "Objeto(s): " 
                + objetos + ".\n"
                + "Precio de total: " 
                + precioTotal + ".\n";
    }
    
    
}
