
package businesslayer.oferta;


public class Oferta {
    private String nombreOferente;
    private int puntuacion; 
    private int precio;

    public Oferta() {
    }


    public Oferta(String nombreOferente, int puntuacion, int precio) {
        this.nombreOferente = nombreOferente;
        this.puntuacion = puntuacion;
        this.precio = precio;
    }

    public String getNombreOferente() {
        return nombreOferente;
    }

    public void setNombreOferente(String nombreOferente) {
        this.nombreOferente = nombreOferente;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Oferta: " 
                + "Nombre del Oferente:" 
                + nombreOferente + ".\n" 
                + "Puntuación: " 
                + puntuacion + ".\n"
                + "Precio: " 
                + precio + ".\n";
    }

    
    
    
}
