/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslayer.item;

import businesslayer.dao.DaoFactory;
import cr.ac.ucenfotec.Conector;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 *
 * @author Luis
 */
public class ItemDAO implements IItemDAO{
    
    public String calcularAntiguedad(LocalDate fechaCompra) {
        String antiguedad = "";
        LocalDate fechaActual = LocalDate.now();
        try{
        int years = fechaActual.getYear() - fechaCompra.getYear();
        int months = fechaActual.getMonthValue() - fechaCompra.getMonthValue();
        int days = fechaActual.getDayOfMonth() - fechaCompra.getDayOfMonth();

        if (months < 0 || (months == 0 && days < 0)) {
            years--;
        }

        months *= -1;
        days *= -1;
        antiguedad = "Días: " + days + ". " + "Meses: " + months + ". "
                + "Años: " + years + ".";
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return antiguedad;
    }

    public String validarObjeto(String ID) throws Exception, SQLException{

        String validacion = "";
        String sql = "SELECT idItem FROM Item WHERE idItem = " + "'" + ID + "'";
        ResultSet set;
        try {
            set = Conector.getConnector(DaoFactory.SQLSERVER).ejecutarSQL(sql, false);
            while (set.next()) {

                validacion = set.getString("idItem");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        if (validacion.equalsIgnoreCase("")) {
            return "";

        }

        return validacion;
    }

}
