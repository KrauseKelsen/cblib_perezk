package businesslayer.item;

import java.time.LocalDate;

public class Item {
    private String ID;
    private String nombre;
    private String descripcion;
    private String estado;
    private LocalDate fechaCompra;
    private String antiguedad;

    public Item() {
    }

    public Item(String ID, String nombre, String descripcion,
            LocalDate fechaCompra, String estado) {
        this.ID = ID;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.fechaCompra = fechaCompra;
        this.estado = estado;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public String getAntiguedad() {
        return antiguedad;
    }

    public void setAntiguedad(String antiguedad) {
        this.antiguedad = antiguedad;
    }

    @Override
    public String toString() {
        return "Ítem: " + "\n"
                + "ID: "
                + ID + ".\n"
                + "Nombre: "
                + nombre + ".\n"
                + "Descripcion: "
                + descripcion + ".\n"
                + "Estado: "
                + estado + ".\n"
                + "Fecha de compra: "
                + fechaCompra + ".\n"
                + "Antiguedad: "
                + antiguedad + ".\n";
    }

}
