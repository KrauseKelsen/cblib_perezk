/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslayer.item;

import java.sql.SQLException;
import java.time.LocalDate;

/**
 *
 * @author Luis
 */
public interface IItemDAO {

    public String calcularAntiguedad(LocalDate fechaCompra);
    public String validarObjeto(String ID) throws Exception, SQLException;
}
