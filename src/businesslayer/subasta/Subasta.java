
package businesslayer.subasta;

import businesslayer.item.Item;
import businesslayer.usuario.Usuario;
import java.time.LocalDate;
import java.util.ArrayList;

public class Subasta {
    private Usuario nombreSubastero;
    private LocalDate fechaVence;
    private ArrayList <Item> ventaObjetos;
    private double puntuacion;
    private int precioMinimo;
    private boolean marca;

    public Subasta() {
    }

    public Subasta(Usuario nombreSubastero,
                   LocalDate fechaVence, ArrayList<Item> ventaObjetos, 
                   double puntuacion, int precioMinimo, boolean marca) {
        this.nombreSubastero = nombreSubastero;
        this.fechaVence = fechaVence;
        this.ventaObjetos = ventaObjetos;
        this.puntuacion = puntuacion;
        this.precioMinimo = precioMinimo;
        this.marca = marca;
    }

    public Usuario getNombreSubastero() {
        return nombreSubastero;
    }

    public void setNombreSubastero(Usuario nombreSubastero) {
        this.nombreSubastero = nombreSubastero;
    }
    
    

    public LocalDate getFechaVence() {
        return fechaVence;
    }

    public void setFechaVence(LocalDate fechaVence) {
        this.fechaVence = fechaVence;
    }

    public ArrayList<Item> getVentaObjetos() {
        return ventaObjetos;
    }

    public void setVentaObjetos(ArrayList<Item> VentaObjetos) {
        this.ventaObjetos = VentaObjetos;
    }

    public double getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(double puntuacion) {
        this.puntuacion = puntuacion;
    }

    public int getPrecioMinimo() {
        return precioMinimo;
    }

    public void setPrecioMinimo(int precioMinimo) {
        this.precioMinimo = precioMinimo;
    }

    public boolean isMarca() {
        return marca;
    }

    public void setMarca(boolean marca) {
        this.marca = marca;
    }

    @Override
    public String toString() {
        return "Subasta: " 
                + "Nombre de subastero: "
                + nombreSubastero + ".\n"
                + "Fecha en la cual vence: " 
                + fechaVence + ".\n"
                + "Objeto(s) a la venta: " 
                + ventaObjetos + ".\n"
                + "Puntuación: " 
                + puntuacion + ".\n"
                + "Precio Mínimo a pujar: " 
                + precioMinimo + ".\n"
                + "Marca del moderador: " 
                + marca + ".\n";
    }
    
    
    
}
