
package businesslayer.usuario;

import businesslayer.usuario.Usuario;
import java.time.LocalDate;
import java.util.ArrayList;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


public class Coleccionista extends Usuario{
    private int puntuacion;
    private String provincia;
    private String canton;
    private String distrito;
    private String direccion;
//    private ArrayList <String> intereses;
//    private ArrayList <Item> objetosPropios;

    public Coleccionista(String nombre, String identificacion, 
                          LocalDate bornIn, String correo, 
                          String password, String provincia,
                          String canton, String distrito, String direccion,
//                          ArrayList <String> intereses, ArrayList<Item> objetosPropios, 
                          String tipo, String estado) {
        super (nombre, identificacion, bornIn, correo, password, tipo, estado);
        this.provincia = provincia;
        this.canton = canton;
        this.distrito = distrito;
        this.direccion = direccion;
//        this.intereses = intereses;
//        this.objetosPropios = objetosPropios;
        this.tipo = tipo;
        
    }
    
    public Coleccionista (){
        super ();
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }
    
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
//    public ArrayList<String> getIntereses() {
//        return intereses;
//    }
//
//    public void setIntereses(ArrayList<String> intereses) {
//        this.intereses = intereses;
//    }
//
//    public ArrayList<Item> getObjetosPropios() {
//        return objetosPropios;
//    }
//
//    public void setObjetosPropios(ArrayList<Item> objetosPropios) {
//        this.objetosPropios = objetosPropios;
//    }

    @Override
    public String toString() {
        return super.toString() 
                + "Puntuación: " 
                + puntuacion + ".\n"
                + "Provincia: " 
                + provincia + ".\n"
                + "Cantón: " 
                + canton + ".\n"
                + "Distrito: " 
                + distrito + ".\n"
                + "Dirección: " 
                + direccion + ".\n"
//                + "Intereses: " 
////                + intereses + ".\n"
//                + "Objetos: " 
//                + objetosPropios + ".\n"
                ;
    }

    
    

    

    
    
    
}
