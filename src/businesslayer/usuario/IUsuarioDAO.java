/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslayer.usuario;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Luis
 */
public interface IUsuarioDAO {

    public String moderadorExistente() throws java.sql.SQLException, Exception;

    public String validarUsuario(String identificacion) throws Exception, SQLException;

    public String validarCorreo(String correo) throws Exception, SQLException;
    
    public int calcularEdad(LocalDate fechaNacimiento) throws java.sql.SQLException, Exception;
    
    public void registrarUsuario(String nombre, String identificacion,
    LocalDate bornIn, int edad, String correo, String password, String tipo, String estado)
    throws java.sql.SQLException, Exception;

    public void registrarModerador(String identificacion) throws java.sql.SQLException, Exception;

    public void registrarColeccionista(String identificacion, int puntuacion, String provincia,
    String canton, String distrito, String direccion) throws java.sql.SQLException, Exception;

    public void registrarVendedor(String identificacion, int puntuacion, String provincia,
    String canton, String distrito, String direccion) throws java.sql.SQLException, Exception;
    
    public ArrayList<Usuario> listarUsuario() throws java.sql.SQLException, Exception; 
    
    public void modificarEstadoUsuario(String identificacion, String estado);
    
    public int validarLogin(String correo, String contrasenna);
    
    public String tipoUsuarioLogin(String correo, String contrasenna);
     
}
