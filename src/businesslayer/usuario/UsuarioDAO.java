/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslayer.usuario;

import businesslayer.dao.DaoFactory;
import cr.ac.ucenfotec.Conector;
import datalayer.Conexion;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Luis
 */
public class UsuarioDAO implements IUsuarioDAO {

    public UsuarioDAO() {
    }

    public String moderadorExistente() throws Exception, SQLException {
        String validacion = "";
        String sql = "SELECT * FROM Moderador";
        ResultSet set;
        try {
            set = Conector.getConnector(DaoFactory.SQLSERVER).ejecutarSQL(sql, false);
            while (set.next()) {

                validacion = set.getString("idUsuario");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        if (!"".equals(validacion)) {
            return validacion;
        }
        return validacion;
    }

    public String validarUsuario(String identificacion) throws Exception, SQLException {
        String validacion = "";
        String sql = "SELECT identificacion FROM Usuario WHERE identificacion = "
                + "'" + identificacion + "'";
        ResultSet set;
        try {
            set = Conector.getConnector(DaoFactory.SQLSERVER).ejecutarSQL(sql, false);
            while (set.next()) {

                validacion = set.getString("identificacion");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        if (validacion.equalsIgnoreCase("")) {
            return "";

        }
        return validacion;
    }

    public String validarCorreo(String correo) throws Exception, SQLException {
        String validacion = "";
        String sql = "SELECT correo FROM Usuario WHERE correo = " + "'" + correo + "'";
        ResultSet set;
        try {
            set = Conector.getConnector(DaoFactory.SQLSERVER).ejecutarSQL(sql, false);
            while (set.next()) {

                validacion = set.getString("correo");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        if (validacion.equalsIgnoreCase("")) {
            return "";

        }
        return validacion;
    }

    public int calcularEdad(LocalDate fechaNacimiento) throws Exception, SQLException {
        int years = 0;
        Date miDate = Date.valueOf(fechaNacimiento);
        String sql = "SELECT [dbo].[fn_calcularEdad]" + "('" + miDate + "')";
        ResultSet set;
        try {
            set = Conector.getConnector(DaoFactory.SQLSERVER).ejecutarSQL(sql, false);
            while (set.next()) {

                years = set.getInt("");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return years;
    }

    public void registrarUsuario(String nombre, String identificacion,
            LocalDate bornIn, int edad, String correo, String password,
            String tipo, String estado)throws java.sql.SQLException, Exception {
        Date miDate = Date.valueOf(bornIn);
        String sql = "INSERT INTO Usuario "
                + "(identificacion, nombre, fechaNacimiento, edad, correo, contrasenna, tipo, estadoUsuario)VALUES "
                + "(" + "'" + identificacion + "'" + "," + "'" + nombre + "'" + "," + "'" + miDate + "'"
                + "," + edad + "," + "'" + correo + "'" + "," + "'" + password + "'" 
                + "," + "'" + tipo + "'" + "," + "'" + estado + "'" + ")";
        try {
            Conector.getConnector(DaoFactory.SQLSERVER).ejecutarSQL(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void registrarModerador(String identificacion) throws Exception, SQLException {

        String sql = "INSERT INTO Moderador (idUsuario) VALUES " + "('" + identificacion + "')";
        try {
            Conector.getConnector(DaoFactory.SQLSERVER).ejecutarSQL(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void registrarColeccionista(String identificacion, int puntuacion,
            String provincia, String canton, String distrito, String direccion)
            throws Exception, SQLException {
        //FALTAN LOS INTERESES
        String sql = "INSERT INTO Coleccionista "
                + "(idUsuario, puntuacion, provincia, canton, distrito, direccion)VALUES "
                + "(" + "'" + identificacion + "'" + "," + puntuacion + "," + "'" + provincia + "'"
                + "," + "'" + canton + "'" + "," + "'" + distrito + "'" + "," + "'" + direccion + "'" + ")";
        try {
            Conector.getConnector(DaoFactory.SQLSERVER).ejecutarSQL(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void registrarVendedor(String identificacion, int puntuacion,
            String provincia, String canton, String distrito, String direccion)
            throws Exception, SQLException {
        String sql = "INSERT INTO Vendedor "
                + "(idUsuario, puntuacion, provincia, canton, distrito, direccion)VALUES "
                + "(" + "'" + identificacion + "'" + "," + puntuacion + "," + "'" + provincia + "'"
                + "," + "'" + canton + "'" + "," + "'" + distrito + "'" + "," + "'" + direccion + "'" + ")";
        try {
            Conector.getConnector(DaoFactory.SQLSERVER).ejecutarSQL(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public ArrayList <Usuario> listarUsuario ()throws Exception, SQLException{
        ArrayList<Usuario> misUsuarios = new ArrayList();
        String sql = "SELECT identificacion, nombre, fechaNacimiento, edad, correo, contrasenna, tipo, estadoUsuario FROM Usuario";
        ResultSet set;
        Date miDate;
        String identificacion, nombre, correo, contrasenna, tipo, estado;
        int edad; 
        LocalDate bornIn;
        
        try{
            set = Conector.getConnector(DaoFactory.SQLSERVER).ejecutarSQL(sql, false);
            while (set.next()) {
                Usuario miUsuario = new Usuario();
                identificacion = set.getString("identificacion");
                nombre = set.getString("nombre");
                miDate = set.getDate("fechaNacimiento");
                edad = set.getInt("edad");
                bornIn = miDate.toLocalDate();
                correo = set.getString("correo");
                contrasenna = set.getString("contrasenna");
                tipo = set.getString("tipo");
                estado = set.getString("estadoUsuario");
                miUsuario.setIdentificacion(identificacion);
                miUsuario.setNombre(nombre);
                miUsuario.setBornIn(bornIn);
                miUsuario.setCorreo(correo);
                miUsuario.setEdad(edad);
                miUsuario.setPassword(contrasenna);
                miUsuario.setTipo(tipo);
                miUsuario.setEstado(estado);
                misUsuarios.add(miUsuario);
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return misUsuarios;
    }
    
    public void modificarEstadoUsuario(String identificacion, String estado){
        if (estado.equals("Habilitado")) {
            estado = "Deshabilitado";        
        }else if (estado.equals("Deshabilitado")){
            estado = "Habilitado";
        }
        String sql = "UPDATE Usuario SET estadoUsuario =" 
        + "'" + estado + "'" + "WHERE identificacion =" + "'" + identificacion + "'";
        try{
            Conector.getConnector(DaoFactory.SQLSERVER).ejecutarSQL(sql);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public int validarLogin(String correo, String contrasenna){
        String status = "";
        String email = "";
        String password = "";
        String sql = "SELECT correo, contrasenna, estadoUsuario FROM Usuario WHERE correo ="
                + "'"+correo+"'" + " AND " + "contrasenna =" + "'"+contrasenna+"'";
        ResultSet set;
        try{
            set = Conector.getConnector(DaoFactory.SQLSERVER).ejecutarSQL(sql, false);
            while (set.next()) {
                status = set.getString("estadoUsuario");
                email = set.getString("correo");
                password = set.getString("contrasenna");
            }
            if (status.equals("Deshabilitado")) {
                return -1;
            }else if(status.equals("Habilitado") && !"".equals(email) && !"".equals(password)){
                return -2;
            }else if(email.equals("") && password.equals("")){
                return -3;
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return 0;
    }
    
    public String tipoUsuarioLogin(String correo, String contrasenna){
        String status = "";
        String tipo = "";
        String sql = "SELECT estadoUsuario, tipo FROM Usuario WHERE correo ="
                + "'"+correo+"'" + " AND " + "contrasenna =" + "'"+contrasenna+"'";
        ResultSet set;
        try{
            set = Conector.getConnector(DaoFactory.SQLSERVER).ejecutarSQL(sql, false);
            while (set.next()) {
                status = set.getString("estadoUsuario");
                tipo = set.getString("tipo");
            }
            if (status.equals("Deshabilitado")) {
                return "Deshabilitado";
            }else if(status.equals("Habilitado")){                
                return tipo;
            }else if(status.equals("") && tipo.equals("")){
                return "";
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return tipo;
    }
}
