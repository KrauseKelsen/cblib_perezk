
package businesslayer.usuario;

import java.time.LocalDate;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Usuario {
    protected String nombre;
    protected String identificacion;
    protected Integer edad;
    protected String correo;
    protected String password;
    protected LocalDate bornIn;
    protected String tipo;
    protected String estado;

    public Usuario(String nombre, String identificacion, LocalDate bornIn, String correo, String password,
            String tipo, String estado) {
        this.nombre = nombre;
        this.identificacion = identificacion;
        this.bornIn = bornIn;
        this.edad = edad;
        this.correo = correo;
        this.password = password;
        this.tipo = tipo;
        this.estado = estado;
    }

    public Usuario() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public LocalDate getBornIn() {
        return bornIn;
    }

    public void setBornIn(LocalDate bornIn) {
        this.bornIn = bornIn;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    

    @Override
    public String toString() {
        return "Usuario: " 
                + "nombre=" 
                + nombre + ".\n"
                + "Identificacion: " 
                + identificacion + ".\n" 
                + "Nacido en: " 
                + bornIn + ".\n"
                + "Edad: " 
                + edad + ".\n"
                + "Correo: " 
                + correo + ".\n"
                + "Contraseña: " 
                + password + ".\n"
                + "Tipo: "
                + tipo + ".\n"
                + "Estado: "
                + estado;
    }
    
    
    
}
