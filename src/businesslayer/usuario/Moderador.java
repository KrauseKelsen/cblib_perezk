
package businesslayer.usuario;

import businesslayer.usuario.Usuario;
import java.time.LocalDate;

public class Moderador extends Usuario{

    public Moderador() {
        super();
    }

    public Moderador(String nombre, String identificacion, LocalDate bornIn, 
                     int edad, String correo, String password, String tipo,
                     String estado) {
        super(nombre, identificacion, bornIn, correo, password, tipo, estado);
    }

    @Override
    public String toString() {
        return "Moderador: " 
                + super.toString();
    }
    
    
    
}