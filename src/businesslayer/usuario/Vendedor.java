

package businesslayer.usuario;

import businesslayer.usuario.Usuario;
import java.time.LocalDate;


public class Vendedor extends Usuario {
    private int puntuacion;
    private String provincia;
    private String canton;
    private String distrito;
    private String direccion;
    public Vendedor() {
        super ();
    }

    public Vendedor(String nombre, String identificacion, LocalDate bornIn, 
                    int edad, String correo, String password, String tipo, 
                    String estado, int puntuacion, String provincia, 
                    String canton, String distrito, String direccion) {
        super(nombre, identificacion, bornIn, correo, password, tipo, estado);
        this.puntuacion = puntuacion;
        this.provincia = provincia;
        this.canton = canton;
        this.distrito = distrito;
        this.direccion = direccion;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }
    
    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return super.toString()
                + "Puntuación: " 
                + puntuacion + ".\n"
                + "Provincia: " 
                + provincia + ".\n"
                + "Cantón: " 
                + canton + ".\n"
                + "Distrito: " 
                + distrito + ".\n"
                + "Dirección: " 
                + direccion;
    }

    
    
    
    
}
