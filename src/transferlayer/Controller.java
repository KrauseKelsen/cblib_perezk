package transferlayer;

import businesslayer.dao.DaoFactory;
import businesslayer.item.IItemDAO;
import businesslayer.usuario.Coleccionista;
import businesslayer.oferta.Oferta;
import businesslayer.usuario.IUsuarioDAO;
import businesslayer.usuario.Usuario;
import datalayer.CL;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class Controller {

    private final CL logica = new CL();

    public Controller() {

    }

    public String moderadorExistente() {
        String confirmado = "";
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IUsuarioDAO dao = factory.getUsuarioDAO();
            confirmado = dao.moderadorExistente();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        if (!"".equals(confirmado)) {
            return confirmado;
        }
        return confirmado;
    }

    public int registroObjeto(String identificacion, String ID, String nombre,
            String descripcion, LocalDate fechaCompra, String estado) {
        String validarObjeto = "";
        String antiguedad = "";
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IItemDAO dao = factory.getItemDAO();
            validarObjeto = dao.validarObjeto(ID);
            antiguedad = dao.calcularAntiguedad(fechaCompra);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        if (validarObjeto.equals("")) {
            logica.registrarObjeto(identificacion, ID, nombre, descripcion,
                    fechaCompra, antiguedad, estado);
            return -1;
        } else {
            return -2;
        }
    }

    public int registroOferta(Coleccionista miColeccionista,
            int oferta) {
        int registro = 1;
        Oferta miOferta = new Oferta();
        String nombreOferente = miColeccionista.getNombre();
        int puntuacion = miColeccionista.getPuntuacion();
        miOferta.setNombreOferente(nombreOferente);
        miOferta.setPuntuacion(puntuacion);
        miOferta.setPrecio(oferta);
        logica.registrarOferta(miOferta);

        return registro;
    }

    public int registrarUsuario(String nombre, String identificacion,
            LocalDate bornIn, String correo,
            String password, int puntuacion, String provincia,
            String canton, String distrito, String direccion,
            //            ArrayList<String> intereses, ArrayList<Item> objetosPropios, 
            String tipo, String estado) throws IOException, SQLException, Exception {

        String validacionUsuario;
        String validacionCorreo;
        int edad;
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IUsuarioDAO dao = factory.getUsuarioDAO();
            edad = dao.calcularEdad(bornIn);
            if (edad < 18) {
                return -2;
            } else if (edad >= 18) {
                validacionUsuario = dao.validarUsuario(identificacion);
                validacionCorreo = dao.validarCorreo(correo);
                if (validacionUsuario.equals("") && validacionCorreo.equals("")) {
                    dao.registrarUsuario(nombre, identificacion,
                            bornIn, edad, correo, password, tipo, estado);
                    switch (tipo) {
                        case "Coleccionista":
                            dao.registrarColeccionista(identificacion, puntuacion,
                                    provincia, canton, distrito, direccion);
                            return -1;
                        case "Vendedor":
                            dao.registrarVendedor(identificacion, puntuacion,
                                    provincia, canton, distrito, direccion);
                            return -1;
                        case "Moderador":
                            dao.registrarModerador(identificacion);
                            return -1;
                        default:
                            break;
                    }
                } else if (!"".equals(validacionUsuario)) {
                    return -3;
                } else if (!"".equals(validacionCorreo)) {
                    return -4;
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return 0;
    }

    public String edadValidada(LocalDate bornIn) throws IOException {
        String edad = "";
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IUsuarioDAO dao = factory.getUsuarioDAO();
            int edadValidada = dao.calcularEdad(bornIn);
            edad = Integer.toString(edadValidada);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return edad;
    }

    public String antiguedadValidada(LocalDate fechaCompra) {
        String antiguedad = "";
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IItemDAO dao = factory.getItemDAO();
            antiguedad = dao.calcularAntiguedad(fechaCompra);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return antiguedad;
    }

    public ArrayList<Usuario> listarUsuario() {
        ArrayList<Usuario> lista = new ArrayList();
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IUsuarioDAO dao = factory.getUsuarioDAO();
            lista = dao.listarUsuario();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return lista;
    }

    public void modificarEstadoUsuario(String identificacion, String estado) {

        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IUsuarioDAO dao = factory.getUsuarioDAO();
            dao.modificarEstadoUsuario(identificacion, estado);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public int validarLogin(String correo, String contrasenna) {
        int validacion = 0;
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IUsuarioDAO dao = factory.getUsuarioDAO();
            validacion = dao.validarLogin(correo, contrasenna);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return validacion;
    }

    public String retornarTipoUsuario(String correo, String contrasenna) {
        String tipoUsuario = "";
        try {
            DaoFactory factory = DaoFactory.getDaoFactory(DaoFactory.SQLSERVER);
            IUsuarioDAO dao = factory.getUsuarioDAO();
            tipoUsuario = dao.tipoUsuarioLogin(correo, contrasenna);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return tipoUsuario;
    }

}
